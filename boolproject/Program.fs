﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.

module Main

open System.Windows.Forms

open System.Windows.Forms
open System.Drawing
open FParsec
open BoolCore

let generateEditWindow bool =
    let form = new Form(BackColor = Color.DeepPink, Text = print_boolean bool)
    let mutable boolhistory = [bool]
    let mutable highlight_directions = []
    let mutable highlight_on = false
    let mutable selected_rule = Rules.rule9
    let bool_label = 
        let temp = new Label()
        do temp.Location <- new Point(25,25)
        temp
    let update_label() =
        do bool_label.Text <- if highlight_on
                              then print_boolean (BoolCore.highlight_directions (List.head boolhistory) highlight_directions)
                              else print_boolean (List.head boolhistory)
        
    let button_AddHighlight =
        let temp = new Button()
        do temp.Location <- new Point(25,100)
        do temp.Text <- "Add Highlight"
        do temp.Click.Add(fun e -> highlight_on <- not highlight_on
                                   update_label())
        do temp.Size <- new Size(100,25)
        temp
    let button_ApplyRule =
        let temp = new Button()
        do temp.Location <- new Point(25,125)
        do temp.Text <- "Apply Rule"
        do temp.Click.Add(fun e -> boolhistory <- match boolhistory with
                                                  | x::rest -> (fst <| selected_rule x)::x::rest
                                                  | [] -> []
                                   update_label())
        temp
    update_label()
    form.Controls.AddRange([|bool_label; button_AddHighlight; button_ApplyRule|])
    form

module EntryWindow =

    let textbox = 
        let temp = new TextBox()
        do temp.Location <- new Point(25, 25)
        do temp.Size <- new Size(500,50)
        do temp.Text <- "Enter statement here..."
        temp
    let outputbox =
        let temp = new Label()
        do temp.Location <- new Point(25, 75)
        do temp.Size <- new Size(400, 100)
        do temp.Text <- "Output: "
        temp
    let updateOutputbox text =
        do outputbox.Text <- "Output: " + text
        ()
    let enterbutton =
        let temp = new Button()
        do temp.Location <- new Point(25,50)
        do temp.Text <- "Parse"
        // System.Diagnostics.Debug.WriteLine
        let printOrError = function
            | None -> "Error!"
            | Some boolean -> print_boolean boolean
        do temp.Click.Add(fun e -> let parsed = run Parsing.boolexpr textbox.Text
                                   match parsed with
                                   | Success(result, _, _) -> (BoolEditWindow.generateBoolWindow (result)).ShowDialog() |> ignore
                                                              do updateOutputbox ""
                                   | ParserResult.Failure(msg, _, _) -> do updateOutputbox msg)
                                   //updateOutputbox ("Output: " + (printOrError parsed)))
        temp
    let form = new Form(BackColor = Color.CornflowerBlue, Text = "Boolean Algebra Project")
    do form.AutoSize <- true
    form.Controls.AddRange([|textbox; enterbutton; outputbox|])

[<EntryPoint>]
let main argv = 
    
    Application.Run(BoolEntryWindow.entryWindow)
    0 // return an integer exit code