﻿module Parsing

open FParsec
open System
open BoolCore

let ok = 1

let boolexpr_NEW : Parser<_,unit> =
    let sepBy2 term sep =
        (term .>> sep .>>. sepBy1 term sep) |>> fun (x, xs) -> x::xs
    let bracketed p = between (pstring "(" <?> "Open Bracket") (pstring ")" <|> failFatally "Closing Bracket") p

    let boolexpr, boolexpr_ref = createParserForwardedToRef<_,_>()
    //let fl = failwith "Parser encountered an impossible failure."

    //let term = (asciiUpper |>> fun x -> Char.ToString x) <|> ((pchar '!') >>. asciiUpper |>> fun x -> String.Format("!{0}", Char.ToString x))
    let var = ((asciiUpper |>> Variable) <?> "Single ascii Upper ") <|> ((anyOf "10" |>> function | '1' -> Constant true | '0' -> Constant false) <?> "Constant")

    // A term is either a bracketed expression, a variable, or a not.
    let term = bracketed (boolexpr) <|> var <|> (pstring "!" >>. (var <|> bracketed boolexpr) |>> Not)

    // An and term is a sequence of terms seperated by "."
    let and_terms = sepBy2 term (pstring ".") |>> And
    // An or term is a sequence of terms and and terms seperated by "+". 
    let or_terms = sepBy2 (attempt (term .>> notFollowedBy (pstring ".")) <|> and_terms) (pstring "+") |>> Or

    // A boolean expression can either be an or, and or a term.
    do boolexpr_ref := choice [or_terms; and_terms; term]
    boolexpr

// This is the main parser.
let boolexpr : Parser<_,unit> =
    let sepBy2 term sep =
        (term .>> sep .>>. sepBy1 term sep) |>> fun (x, xs) -> x::xs
    let bracketed p = between (pstring "(" <?> "Open Bracket") (pstring ")" <|> failFatally "Closing Bracket") p

    let boolexpr, boolexpr_ref = createParserForwardedToRef<_,_>()
    //let fl = failwith "Parser encountered an impossible failure."

    //let term = (asciiUpper |>> fun x -> Char.ToString x) <|> ((pchar '!') >>. asciiUpper |>> fun x -> String.Format("!{0}", Char.ToString x))
    let var = ((asciiUpper |>> Variable) <?> "Single ascii Upper ") <|> ((anyOf "10" |>> function | '1' -> Constant true | '0' -> Constant false) <?> "Constant")

    // A term is either a bracketed expression, a variable, or a not.
    let term = bracketed boolexpr <|> var <|> (pstring "!" >>. (var <|> bracketed boolexpr) |>> Not)

    // An and term is a sequence of terms seperated by "."
    let and_terms = sepBy2 term (pstring ".") |>> And
    // An or term is a sequence of terms and and terms seperated by "+". 
    let or_terms = sepBy2 (attempt (term .>> notFollowedBy (pstring ".")) <|> and_terms) (pstring "+") |>> Or

    // A boolean expression can either be an or, and or a term.
    do boolexpr_ref := choice [attempt or_terms; attempt and_terms; attempt term]
    boolexpr .>> eof 
    // eof = end of file
    // the eof makes sure that the parser uses up all the input
    // if not it will throw an error.