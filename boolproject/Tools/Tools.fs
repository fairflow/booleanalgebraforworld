﻿module Tools

///A Datatype which can store an undirected graph.
module Graph =
    type Graph<'a when 'a : comparison> = {
        nodes: Set<'a>
        connections: Set<('a * 'a) * int>
    }

    let empty<'a when 'a : comparison> = {nodes = Set.empty<'a> ; connections = Set.empty<('a * 'a) * int>}

    let get_nodes ({nodes = n}) = n
    let get_connections ({connections = c}) = c

    
    let add node graph =
        {graph with nodes = Set.add node graph.nodes} 

    let remove node ({nodes = n ; connections = c}) = 
        let n' = Set.remove node n
        let c' = Set.filter (fun ((n1,n2), _) -> n1 = node || n2 = node) c
        {nodes = n'; connections = c'}

    let within node ({nodes = n}) =
        Set.exists ((=) node) n

    let connected node1 node2 ({connections = c}) = 
        Set.exists (fun ((n1,n2), _) -> (n1 = node1 && n2 = node2) || (n1 = node2 && n2 = node1)) c

    let disconnect n1 n2 ({nodes = n ; connections = c}) =
        if connected n1 n2 ({nodes = n ; connections = c})
        then let c' = Set.filter (fun ((n1,n2), _) -> not (n1 = n1 && n2 = n2 || n1 = n2 && n2 = n1)) c
             ({nodes = n ; connections = c'})
        else ({nodes = n ; connections = c})

    let connect n1 n2 distance ({nodes = n ; connections = c}) =
        let ({nodes = n' ; connections = c'}) = if connected n1 n2 ({nodes = n ; connections = c})
                                                then disconnect n1 n2 ({nodes = n ; connections = c})
                                                else ({nodes = n ; connections = c})
        {nodes = n' |> Set.add n1 |> Set.add n2 ; connections = c' |> Set.add ((n1, n2), distance)}
    
    let connected_nodes node ({nodes = n ; connections = c}) =
        Set.filter (fun ((a,b),_) -> a = node || b = node) c |> Set.map (fun ((a,b),_) -> if a = node then b else a)
    
    let connected_nodes_with_weights node ({nodes = n ; connections = c}) =
        Set.filter (fun ((a,b),_) -> a = node || b = node) c |> Set.map (fun ((a,b),d) -> if a = node then (b,d) else (a,d))

    let minimum_distance_vertex normal node graph =
        let connected_nodes = connected_nodes_with_weights node graph
        Set.fold (fun (name, distance) (n', d') -> if distance > d' then (n', d') else (name, distance)) normal connected_nodes

module Queue =
    type Queue<'a> =
        | Cap
        | Node of 'a * Queue<'a>

    let enq item = function
        | Cap -> Node(item,Cap)
        | q -> Node(item, q)

    let deq err = function
        | Cap -> (err, Cap)
        | Node(item, q) -> (item, q)

    let empty = Cap

module PQueue =
    type Queue<'a, 'b when 'b : comparison> =
        | Cap
        | Node of 'a * 'b * Queue<'a, 'b>

    let rec enq item priority = function
        | Cap -> Node(item, priority, Cap)
        | Node(citem, cpriority, rest) -> if cpriority < priority
                                          then Node(citem, cpriority, enq item priority rest)
                                          else Node(item, priority, Node(citem, cpriority, rest))
    
    let deq err = function
        | Cap -> (err, Cap)
        | Node(item, _, q) -> (item, q)

    let deqp err = function
        | Cap -> (err, Cap)
        | Node(item, p, q) -> ((item, p), q)

    let head err = function
        | Cap -> err
        | Node(item, _, _) -> item

    let rec dump queue =
        match queue with
        | Cap -> []
        | Node(item, _, rest) -> item::(dump rest)

    let rec dumpw queue =
        match queue with
        | Cap -> []
        | Node(item, p, rest) -> (item, p)::(dumpw rest)

    let rec removespecific item = function
        | Cap -> Cap
        | Node(citem, p, rest) -> if citem = item
                                  then rest
                                  else Node(citem, p, removespecific item rest)

    let rec update_priority item newpriority pq =
        removespecific item pq |>  enq item newpriority

    let empty = Cap

type Distance = | Distance of int | Infinity

let dijkstras source_node graph =
    let (+) distance1 distance2 =
        match (distance1, distance2) with
        | (Infinity, Infinity) -> Infinity
        | (_, Infinity) | (Infinity, _) -> Infinity
        | (Distance a, Distance b) -> Distance (a+b)
    let mutable q = Set.fold (fun q node -> PQueue.enq node Infinity q) (PQueue.empty) (Graph.get_nodes graph) |> PQueue.update_priority source_node (Distance 0)
    let mutable distance = Graph.get_nodes graph |> Set.toList |> List.map (fun x -> (x, Infinity)) |> Map.ofList
    let mutable previous = Graph.get_nodes graph |> Set.toList |> List.map (fun x -> (x, None)) |> Map.ofList
    do distance <- Map.add source_node (Distance 0) distance
    while (q <> PQueue.empty)
        do let current_node = PQueue.deq "error" q |> fst
           q <- PQueue.deq "error" q |> snd
           for (name, distancefrom) in (Graph.connected_nodes_with_weights current_node graph)
               do printf "%s %s\n" current_node name
                  let temp = distance.[current_node] + (Distance distancefrom)
                  if temp < distance.[name]
                  then distance <- Map.add name temp distance
                       q <- PQueue.update_priority name temp q
                       previous <- Map.add name (Some current_node) previous
    (distance, previous)


