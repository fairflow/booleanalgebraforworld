﻿module BoolEntryWindow

open System.Windows.Forms
open System.Drawing
open System.CodeDom.Compiler
open FParsec
open BoolCore

let entryWindow =
    
    // The box that the user enters their statement in.
    let entryBox =
        let temp = new TextBox()
        do temp.Location <- Point(12,12)
        do temp.Size <- Size(238, 20)
        do temp.Name <- "entryBox"
        do temp.TabIndex <- 0
        temp
    //
    let errorBox =
        let temp = new RichTextBox()
        // DockStyle.Fill means that it will fill
        // Whatever container it is placed in.
        do temp.Dock <- DockStyle.Fill
        do temp.Name <- "ErrorBox"
        do temp.TabIndex <- 0
        do temp.Text <- ""
        do temp.ReadOnly <- true
        temp
    // This is a simple function that just updates the 
    // error box's text, and makes the code clearer.
    let updateErrorBox text =
        do errorBox.Text <- "Output: " + text
        ()
    // The button that the user presses to parse their input
    let acceptButton = 
        let temp = new Button()
        do temp.Size <- Size(75, 23)
        do temp.Name <- "AcceptButton"
        do temp.Location <- Point(12, 38)
        do temp.Text <- "Parse"
        do temp.Click.Add(fun e -> // Parse using the boolean expression parser
                                   // the text in the entry box. 
                                   let parsed = run Parsing.boolexpr entryBox.Text
                                   // If the parse is a success, generate a edit window with the boolean result
                                   // And clea the error box.
                                   // If not, update the error box with the error message.
                                   match parsed with
                                   | Success(result, _, _) -> (BoolEditWindow.generateBoolWindow (result)).ShowDialog() |> ignore
                                                              do updateErrorBox ""
                                   | ParserResult.Failure(msg, _, _) -> do updateErrorBox msg)
        temp
    // This is a winforms 'container' that has two panels that 
    // I can place components into that will be seperated, and
    // has a line that seperates the two.
    let splitContainer =
        let temp = new SplitContainer()
        do temp.Dock <- DockStyle.Fill
        do temp.Location <- Point(0,0)
        do temp.Name <- "SplitContainer"
        do temp.Orientation <- Orientation.Horizontal
        do temp.Size <- Size(262, 166)
        do temp.SplitterDistance <- 83
        do temp.TabIndex <- 1
        do temp.Panel1.Controls.Add(entryBox)
        do temp.Panel1.Controls.Add(acceptButton)
        do temp.Panel2.Controls.Add(errorBox)
        temp
    let form = new Form()
    do form.AutoScaleDimensions <- new SizeF(6.F, 13.F)
    do form.AutoScaleMode <- AutoScaleMode.Font
    do form.ClientSize <- new Size(262, 166)
    do form.Controls.Add(splitContainer)
    do form.Name <- "EntryWindow"
    do form.Text <- "BoolProject"
    do splitContainer.Panel1.ResumeLayout(false)
    do splitContainer.Panel1.PerformLayout()
    do splitContainer.Panel2.ResumeLayout(false)
    do splitContainer.EndInit()
    do splitContainer.ResumeLayout(false)
    do form.ResumeLayout(false)
    form