﻿module BoolEditWindow

open System.Windows.Forms
open System.Drawing
open BoolCore

let generateBoolWindow bool =
    let booleanRules = BoolCore.Rules.rules
    let mutable currentBoolList =
        [(bool, None)]
    let mutable boolUndoHistory =
        []
    let mutable currentDirections = 
        []

    let clearUndoHistory() =
        do boolUndoHistory <- []
    let currentBool() =
        currentBoolList.Head

    let RuleSelectionBox =
        let temp = new ComboBox()
        do temp.FormattingEnabled <- true
        do temp.Location <- new Point(3,3)
        do temp.Name <- "RuleSelectionBox"
        do temp.Size <- new Size(180, 21)
        do temp.TabIndex <- 1
        for singlerule in booleanRules do
            do temp.Items.Add(snd singlerule)
        do temp.SelectedIndex <- 0
        temp
    let getCurrentRuleFunction() =
        booleanRules.[RuleSelectionBox.SelectedIndex] |> fst
    let getCurrentRuleIndex() =
        RuleSelectionBox.SelectedIndex
    let getCurrentRuleText() =
        booleanRules.[RuleSelectionBox.SelectedIndex] |> snd
    let undoBackup = []
    let boolHistoryList = 
        let temp = new ListBox()
        do temp.Dock <- DockStyle.Fill
        do temp.FormattingEnabled <- true
        do temp.Location <- new Point(0,0)
        do temp.Name <- "BoolHistoryList"
        do temp.Size <- Size(300,363)
        temp
    let updateBoolHistoryList() =
        do boolHistoryList.BeginUpdate()
        do boolHistoryList.Items.Clear()
        for (bool, rule) in List.rev currentBoolList do
            do boolHistoryList.Items.Add(print_boolean bool + (match rule with | (Some text) -> " | " + text | None -> ""))
        do boolHistoryList.EndUpdate()
    do updateBoolHistoryList()
    let button2 =
        let temp = new Button()
        do temp.Dock <- DockStyle.Fill
        do temp.Location <- Point(0,0)
        do temp.Name <- "UndoButton"
        do temp.Size <- new Size(93,34)
        do temp.TabIndex <- 0
        do temp.Text <- "Undo"
        do temp.UseVisualStyleBackColor <- true
        do temp.Click.Add(fun x -> if currentBoolList.Length > 1
                                   then do boolUndoHistory <- (currentBoolList.Head)::boolUndoHistory
                                        do currentBoolList <- currentBoolList.Tail
                                   do updateBoolHistoryList())
        temp
    let CurrentBoolLabel = 
        let temp = new Label()
        do temp.Dock <- DockStyle.Fill
        do temp.Location <- new Point(3,0)
        do temp.Name <- print_boolean bool
        do temp.Size <- new Size(189, 33)
        do temp.TabIndex <- 0
        do temp.Text <- print_boolean <| fst currentBoolList.Head
        do temp.TextAlign <- ContentAlignment.MiddleCenter
        temp
    let updateCurrentBoolLabel() =
        do CurrentBoolLabel.Text <- (highlight_directions (fst <| currentBool()) currentDirections) |> print_boolean

    // DIRECTIONAL MOVEMENT
    //

    let mutable possibleDirections : (int list * Boolean) list = []

    let directionDropDown =
        let temp = new ComboBox()
        do temp.Name <- "DirectionComboBox"
        do temp.Enabled <- true
        do temp.Size <- new Size(180, 21)
        temp

    let directionalGo =
        let temp = new Button()
        do temp.Name <- "GoButton"
        do temp.Text <- "Go!"
        do temp.Enabled <- true
        temp

    /// Updates the direction dropdown box with the current potential directions
    /// in the possibleDirections mutable variable.
    let updatePossibleDirectionsList() =
        do directionDropDown.BeginUpdate()
        do directionDropDown.Items.Clear()
        for (dirs, bool) in possibleDirections do
            do directionDropDown.Items.Add(print_boolean bool)
        do directionDropDown.EndUpdate()
        if List.length possibleDirections > 0 
        then do directionDropDown.SelectedIndex <- 0

    let updatePossibleDirections() =
        let possibleDirs = BoolCore.getPossibleMovements true (fst <| currentBool()) (currentDirections)
        do possibleDirections <- List.map (fun x -> (x, highlightDirections x (fst <| currentBool()))) possibleDirs

    do directionalGo.Click.Add(fun x -> let index = directionDropDown.SelectedIndex
                                        do currentDirections <- fst possibleDirections.[index]
                                        do updatePossibleDirections()
                                        do updatePossibleDirectionsList()
                                        do updateCurrentBoolLabel())

    updatePossibleDirections()
    updatePossibleDirectionsList()

    let updateBoolWithBrackets totaldir totalBool toBeBracketed =
        do printf "%s\n" (toBeBracketed.ToString())
        let working = BoolCore.followDirections totaldir totalBool
        let savedList = match working with
                        | And l
                        | Or l -> l
                        | _ -> failwith "Impossible Error."
        let bracketedBools = List.choose (fun (exp, bool) -> if bool
                                                             then Some exp
                                                             else None) (List.zip savedList toBeBracketed)
        let unbracketedBools = List.choose (fun (exp, bool) -> if not bool
                                                               then Some exp
                                                               else None) (List.zip savedList toBeBracketed)
        let newBool = match working with
                      | And _ -> And(List.append [And(bracketedBools)] unbracketedBools)
                      | Or _ -> Or(List.append [Or(bracketedBools)] unbracketedBools)
        do currentBoolList <- (replaceBoolAtDirections totaldir totalBool newBool, Some("Bracketing"))::currentBoolList
        do updateBoolHistoryList()
        do clearUndoHistory()
        do updateCurrentBoolLabel()
        do updatePossibleDirections()
        do updatePossibleDirectionsList()

    let createBracketWindow basedir dirs totalBool =
        let form = new Form()
        let mutable toBeBracketed = List.map (fun x -> false) dirs
        let createCheckbox text =
            let temp = new CheckBox()
            do temp.Text <- text
            temp
        let doneButton =
            let temp = new Button()
            do temp.Text <- "Done"
            do temp.Click.Add(fun x -> updateBoolWithBrackets basedir totalBool toBeBracketed
                                       do form.Close())
            do temp.Location <- Point(50,50)
            temp
        
        let checkBoxes = List.mapi (fun i x -> (let temp = createCheckbox (highlightDirections x totalBool |> print_boolean)
                                                do temp.Click.Add(fun a -> do toBeBracketed <- List.replace (not toBeBracketed.[i]) i toBeBracketed)
                                                temp, x)) dirs

        let layout =
            let temp = new FlowLayoutPanel()
            do temp.Name <- "BracketLayout"
            do temp.AutoSizeMode <- AutoSizeMode.GrowAndShrink
            do temp.FlowDirection <- FlowDirection.TopDown
            do temp.ImeMode <- ImeMode.NoControl
            for (cb,_) in checkBoxes do 
                do temp.Controls.Add(cb)
            do temp.Controls.Add(doneButton)
            temp
        do form.Controls.Add(layout)
        form

    let bracketButton =
        let temp = new Button()
        do temp.Name <- "BracketButton"
        do temp.Text <- "Bracket"
        do temp.Click.Add(fun x -> let selectedBool = followDirections currentDirections (fst <| currentBool())
                                   match selectedBool with
                                   | Not _
                                   | Variable _ 
                                   | Constant _ -> ()
                                   | _ -> (createBracketWindow currentDirections (BoolCore.getPossibleMovements false (fst <| currentBool()) currentDirections) (fst <| currentBool())).ShowDialog() |> ignore)
        temp

    let DirectionLayout =
        let temp = new FlowLayoutPanel()
        do temp.Name <- "MovementLayout"
        do temp.AutoSizeMode <- AutoSizeMode.GrowAndShrink
        do temp.FlowDirection <- FlowDirection.TopDown
        do temp.ImeMode <- ImeMode.NoControl
        do temp.Controls.Add(directionDropDown)
        do temp.Controls.Add(directionalGo)
        do temp.Controls.Add(bracketButton)
        temp
    ///


    let redoButton =
        let temp = new Button()
        do temp.Dock <- DockStyle.Fill
        do temp.Location <- Point(0,0)
        do temp.Name <- "RedoButton"
        do temp.Size <- new Size(93,34)
        do temp.TabIndex <- 0
        do temp.Text <- "Redo"
        do temp.UseVisualStyleBackColor <- true
        do temp.Click.Add(fun x -> if boolUndoHistory.Length > 0
                                   then do currentBoolList <- (boolUndoHistory.Head)::currentBoolList
                                        do boolUndoHistory <- boolUndoHistory.Tail
                                   do updateBoolHistoryList()
                                   do updatePossibleDirections()
                                   do updatePossibleDirectionsList())
        temp
    let SplitContainerUndoRedo =
        let temp = new SplitContainer()
        do temp.Dock <- DockStyle.Fill
        do temp.Location <- new Point(3,326)
        do temp.Panel1.Controls.Add(button2)
        do temp.Panel2.Controls.Add(redoButton)
        do temp.Size <- new Size(189,34)
        do temp.SplitterDistance <- 93
        do temp.TabIndex <- 2
        temp
    let ApplyRule =
        let temp = new Button()
        do temp.Location <- new Point(3,30)
        do temp.Name <- "ApplyRule"
        do temp.Size <- new Size(75, 23)
        do temp.TabIndex <- 0
        do temp.Text <- "Apply Rule"
        do temp.Enabled <- true 
        //do temp.TextAlign <- ContentAlignment.MiddleCenter
        do temp.UseVisualStyleBackColor <- true
        do temp.Click.Add(fun x -> let directions = currentDirections
                                   let result = BoolCore.use_rule_with_directions (fst currentBoolList.Head) directions (fun x -> getCurrentRuleFunction() x)
                                   if snd result
                                   then do currentDirections <- []
                                        do currentBoolList <- (fst result, Some(getCurrentRuleText()))::currentBoolList
                                        do updateBoolHistoryList()
                                        do clearUndoHistory()
                                        do updateCurrentBoolLabel()
                                        do updatePossibleDirections()
                                        do updatePossibleDirectionsList())
        temp
        
    
    let ControlFlowLayout =
        let temp = new FlowLayoutPanel()
        do temp.AutoSizeMode <- AutoSizeMode.GrowAndShrink
        do temp.Controls.Add(RuleSelectionBox)
        do temp.Controls.Add(ApplyRule)
        do temp.Controls.Add(DirectionLayout)
        do temp.Dock <- DockStyle.Fill
        do temp.Enabled <- true
        do temp.FlowDirection <- FlowDirection.TopDown
        do temp.ImeMode <- ImeMode.NoControl
        do temp.Location <- new Point(3,36)
        do temp.Size <- new Size(189, 284)
        do temp.TabIndex <- 1
        temp
    let tableLayoutPanel = 
        let temp = new TableLayoutPanel()
        do temp.ColumnCount <- 1
        do temp.RowCount <- 3
        do temp.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0F))
        do temp.Controls.Add(CurrentBoolLabel, 0, 0)
        do temp.Controls.Add(ControlFlowLayout, 0, 1)
        do temp.Controls.Add(SplitContainerUndoRedo, 0, 2)
        do temp.Dock <- DockStyle.Fill
        do temp.Location <- Point(0,0)
        do temp.Name <- "TableLayoutPanel"
        do temp.RowStyles.Add(new RowStyle(SizeType.Percent, 10.25F))
        do temp.RowStyles.Add(new RowStyle(SizeType.Percent, 89.76F))
        do temp.RowStyles.Add(new RowStyle(SizeType.Percent, 39.0F))
        do temp.Size <- new Size(195, 363)
        do temp.TabIndex <- 0
        temp
    let splitContainerMain =
        let temp = new SplitContainer()
        do temp.Dock <- DockStyle.Fill
        do temp.FixedPanel <- FixedPanel.Panel1
        do temp.Location <- new Point(0,0)
        do temp.Panel1.Controls.Add(boolHistoryList)
        do temp.Panel2.Controls.Add(tableLayoutPanel)
        do temp.Size <- new Size(499,363)
        do temp.SplitterDistance <- 300
        do temp.TabIndex <- 0
        temp

    let form = new Form()
    do form.AutoScaleDimensions <- new SizeF(6.0F, 13.0F)
    do form.AutoScaleMode <- AutoScaleMode.Font
    do form.ClientSize <- new Size(499, 363)
    do form.Controls.Add(splitContainerMain)
    do form.Name <- "EditWindow"
    do form.Text <- print_boolean bool
    do splitContainerMain.Panel1.ResumeLayout(false)
    do form.ResumeLayout(false)

    form
    
    