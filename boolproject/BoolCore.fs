﻿module BoolCore

module List =
    // from http://www.fssnip.net/7S3/title/Intersperse-a-list
    /// The intersperse function takes an element and a list and
    /// 'intersperses' that element between the elements of the list.
    let intersperse sep ls =
        List.foldBack (fun x -> function
            | [] -> [x]
            | xs -> x::sep::xs) ls []
    // end of copy and paste

    // Returns a new list with the item at index replaced with new_item.
    let replace new_item index list =
        List.mapi (fun i o -> if i = index then new_item else o) list 


    /// Removes the item from a list at a certain index. If the index is
    /// Over the length of the list then the original list will just be returned.
    let rec remove index list =
        match index, list with
        | 0, x::xs -> xs
        | i, x::xs when i > 0 -> remove (i-1) xs
        | _, [] -> []


module String =
    // from http://www.fssnip.net/5u/title/String-explode-and-implode
    /// Converts a string into a list of characters.
    let explode (s:string) =
        [for c in s -> c]
    
    let implode (xs:char list) =
        let sb = System.Text.StringBuilder(xs.Length)
        xs |> List.iter (sb.Append >> ignore)
        sb.ToString()
    // end of copy paste

    let remove_spaces (s:string) =
        Seq.filter ((<>) ' ') s
    
    /// Returns a tuple with the number of left and right brackets in the left and right indexes respectively.
    let count_brackets text =
        let exploded_text = explode text
        let left_brackets = List.filter ((=) '(') exploded_text |> List.length
        let right_brackets = List.filter ((=) ')') exploded_text |> List.length
        (left_brackets, right_brackets)

type Boolean =
    | Constant of bool
    | Variable of char
    | Not of Boolean
    | And of Boolean list
    | Or of Boolean List
    | Highlight of Boolean
// Highlight does nothing, its just so highlighted stuff can be highlighted.
// TODO: Remove 'Highlight' from Boolean and add it to a 'HighlitedBoolean'
type ButtonDirection = 
    | UpArrow | DownArrow | LeftArrow | RightArrow
type Direction = int


module Rules =
    open System.Diagnostics

    // The return type of each rule function is a pair
    // of a Boolean Algebra statement (which is the
    // rule applied to the input)
    //and a boolean
    // true/false value, which is used to return the
    // success of the rule application.
    // These two functions are used to easily add a
    // success or failure tag cleanly.
    let ruleIsSuccess bool = (bool, true)
    let ruleIsFailure bool = (bool, false)

    let rule1 a = 
        match a with
        | And list -> // If a false constant exists in the and then the
                      // input value is equal to true. Else leave as is.
                      if List.exists ((=) (Constant false)) list
                      then Constant false |> ruleIsSuccess
                      else And list |> ruleIsFailure
        | other -> other |> ruleIsFailure

    let rule2 a = 
        match a with
        | And list -> let apply = let b = List.filter ((<>) (Constant true)) list
                                  if List.length b > 1
                                  then And b
                                  else List.head b
                      if apply <> a
                      then apply |> ruleIsSuccess
                      else a |> ruleIsFailure
        | other -> other |> ruleIsFailure
         
    let rule3 a =
        match a with
        | And list -> let applied = let b = Seq.distinct (List.toSeq list) |> List.ofSeq
                                    if List.length b > 1
                                    then And b
                                    else List.head b
                      if applied <> a
                      then applied |> ruleIsSuccess
                      else a |> ruleIsFailure
        | other -> other |> ruleIsFailure

    let rule4 a =
        match a with
        | And list -> // For each item in the list, check if any other is a not but with them inside.
                      let isFalse = List.exists (fun a -> List.exists ((=) (Not a)) list) list
                      if isFalse
                      then Constant false |> ruleIsSuccess
                      else And list |> ruleIsFailure
        | _ -> a |> ruleIsSuccess

    let rule5 a =
        match a with
        | Or list -> let applied = let b = List.filter ((<>) (Constant false)) list
                                   if List.length b > 1
                                   then And b
                                   else List.head b
                     if applied <> a
                     then applied |> ruleIsSuccess
                     else a |> ruleIsFailure
        | other -> other |> ruleIsFailure

    let rule6 a = 
        match a with
        | Or list -> if List.exists ((=) (Constant true)) list
                      then Constant true |> ruleIsSuccess
                      else And list |> ruleIsFailure
        | other -> other |> ruleIsFailure

    let rule7 a = 
        match a with
        | Or list -> let applied = let b = Seq.distinct (List.toSeq list) |> List.ofSeq
                                   if List.length b > 1
                                   then Or b
                                   else List.head b
                     if applied <> a
                     then applied |> ruleIsSuccess
                     else a |> ruleIsFailure
        | other -> other |> ruleIsFailure

    let rule8 a =
        match a with
        | Or list -> // For each item in the list, check if any other is a not but with them inside.
                      let isTrue = List.exists (fun a -> List.exists ((=) (Not a)) list) list
                      if isTrue
                      then Constant true |> ruleIsSuccess
                      else And list |> ruleIsFailure
        | other -> other |> ruleIsFailure

    let rule9 a =
        match a with
        | Not(Not(rest)) -> rest |> ruleIsSuccess
        | other -> other |> ruleIsFailure
    // todo: add 14 -> 17

    let rule12 a =
        let rec f bool_list =
            match bool_list with
            | And(andbool)::rest -> List.append andbool (f rest)
            | other::rest -> other::(f rest)
            | [] -> []
        match a with
        | And(bool) -> And(f bool) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure
    
    let rule13 a =
        let rec f bool_list = 
            match bool_list with 
            | Or(bool)::rest -> List.append bool (f rest) 
            | other::rest -> other::(f rest) 
            | [] -> [] 
        match a with 
        | Or(bool) -> And(f bool) |> ruleIsSuccess 
        | _ -> a |> ruleIsFailure 

    let deMorgans1 a =
        match a with
        | Not(And([b1; b2])) -> Or([Not(b1); Not(b2)]) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure

    let deMorgans2 a =
        match a with
        | Not(Or([b1;b2])) -> And([Not(b1); Not(b2)]) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure

    let deMorgans3 a =
        match a with
        | Or([Not(b1); Not(b2)]) -> Not(And([b1; b2])) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure

    let deMorgans4 a =
        match a with
        | And([Not(b1); Not(b2)]) -> Not(Or([b1;b2])) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure

    (*
    let absorbitionLaw a =
        let containsOr bools =
            List.exists (function | Or(_) -> true | _ -> false) bools
        match a with
        | And(bools) -> if containsOr
                        then *)
    
    let constantRule a =
        match a with
        | Not(Constant value) -> Constant (not value) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure

    let absorbtionLawAnd a =
        match a with
        | And([x;Or([b1;b2])]) when b1 = x || b2 = x -> x |> ruleIsSuccess
        | _ -> a |> ruleIsFailure

    let absorbtionLawOr a =
        match a with
        | Or([x;And([b1;b2])]) when b1 = x || b2 = x -> x |> ruleIsSuccess
        | _ -> a |> ruleIsFailure

    let commutativeLaw a =
        match a with
        | And([b1;b2]) -> And([b2;b1]) |> ruleIsSuccess
        | Or([b1;b2]) -> Or([b2;b1]) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure
    
    let distributiveLaw1 a =
        match a with
        | And([b1; Or([b2; b3])]) -> Or([And([b1; b2]); And([b1; b3])]) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure
    
    let distributiveLaw2 a =
        match a with
        | Or([And([b1; b2]); And([b4; b3])]) when b1 = b4 -> And([b1; Or([b2; b3])]) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure

    let distributiveLaw3 a =
        match a with
        | Or([b1; And([b2; b3])]) -> And([Or([b1; b2]); Or([b1; b3])]) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure

    let distributiveLaw4 a =
        match a with
        | And([Or([b1; b2]); Or([b4; b3])]) when b1 = b4 -> Or([b1; And([b2; b3])]) |> ruleIsSuccess
        | _ -> a |> ruleIsFailure

    let rules = [(rule1, "Rule 1 (A.0 = 0)"); 
                 (rule2, "Rule 2 (A.1 = A)"); 
                 (rule3, "Rule 3 (A.A = A)");
                 (rule4, "Rule 4 (A.!A = 0)");
                 (rule5, "Rule 5 (A+0 = A)"); 
                 (rule6, "Rule 6 (A+1 = 1)"); 
                 (rule7, "Rule 7 (A+A = A)"); 
                 (rule8, "Rule 8 (A+!A = 1)"); 
                 (rule9, "Rule 9 (!(!A) = A)");
                 (rule12, "Rule 12 (A.(B.C) = A.B.C)"); 
                 (rule13, "Rule 13 (A+(B+C) = A+B+C)");
                 (constantRule, "Constant Rule (!1 = 0, !0 = 1)")
                 (commutativeLaw, "Commutative Law");
                 //(rule14, "A.(B+C) = A.B+A.C")
                 (deMorgans1, "De Morgans !(A.B) = !A + !B");
                 (deMorgans2, "De Morgans !(A+B) = !A.!B");
                 (deMorgans3, "De Morgans !A + !B = !(A.B)");
                 (deMorgans4, "De Morgans !A.!B = !(A+B)");
                 (absorbtionLawAnd, "Absorbtion Law (A.(A+B) = A)");
                 (absorbtionLawOr, "Absorbtion Law (A+A.B = A)");
                 (distributiveLaw1, "Distributive Law (A.(B+C) = A.B+A.C)");
                 (distributiveLaw2, "Distributive Law (A+B+A.C = A.(B+C))");
                 (distributiveLaw3, "Distributive Law (A+B.C = (A+B).(A+C))");
                 (distributiveLaw4, "Distributive Law ((A+B).(A+C) = A+B.C))")
                 ]

// Rule 10 -> 13 implemented in the datastructure.



   
let rec highlight_directions (bool:Boolean) (dirs:Direction list) =
    match dirs with
    | 0 :: rest -> match bool with
                    | Not b -> Not (highlight_directions b rest)
                    | And blist -> And (List.replace (highlight_directions (blist.[0]) rest) 0 blist)
                    | Or blist -> Or (List.replace (highlight_directions (blist.[0]) rest) 0 blist)
                    | _ -> Highlight bool
    | x :: rest -> match bool with
                           | And blist -> And (List.replace (highlight_directions (blist.[x]) rest) x blist)
                           | Or blist -> Or (List.replace (highlight_directions (blist.[x]) rest) x blist)
                           | _ -> Highlight bool
    | [] -> Highlight bool

let rec use_rule_with_directions (bool:Boolean) (dirs:Direction list) (rule: Boolean -> Boolean * bool) =
    match dirs with
    | 0 :: rest -> match bool with 
                    | Not b -> let (returnBool, succ) = use_rule_with_directions b rest rule
                               (Not (returnBool), succ)
                    | And blist -> let (returnBool, succ) = use_rule_with_directions (blist.[0]) rest rule
                                   (And (List.replace returnBool 0 blist), succ)
                    | Or blist -> let (returnBool, succ) =use_rule_with_directions (blist.[0]) rest rule
                                  (Or (List.replace returnBool 0 blist), succ)
                    | _ -> failwith "Wrong directions!"
    | x :: rest -> match bool with
                           | And blist -> let (returnBool, succ) = use_rule_with_directions (blist.[x]) rest rule
                                          (And (List.replace returnBool x blist), succ)
                           | Or blist -> let (returnBool, succ) =use_rule_with_directions (blist.[x]) rest rule
                                         (Or (List.replace returnBool x blist), succ)
                           | _ -> failwith "Wrong directions!"
    | [] -> rule bool
            

let rec follow_directions (bool:Boolean) (dirs:Direction list) =
    match dirs with
    | 0 :: rest -> match bool with
                    | Not b -> Not (follow_directions b rest)
                    | And blist -> And (List.replace (follow_directions (blist.[0]) rest) 0 blist)
                    | Or blist -> Or (List.replace (follow_directions (blist.[0]) rest) 0 blist)
                    | _ -> failwith "Wrong directions!"
    | x :: rest -> match bool with
                           | And blist -> And (List.replace (follow_directions (blist.[x]) rest) x blist)
                           | Or blist -> Or (List.replace (follow_directions (blist.[x]) rest) x blist)
                           | _ -> failwith "Wrong directions!"
    | [] -> bool

let rec get_bool_from_directions (bool:Boolean) (dirs:Direction list) =
    match dirs with
    | 0 :: rest -> match bool with
                    | Not b -> get_bool_from_directions b rest
                    | And blist -> get_bool_from_directions (blist.[0]) rest
                    | Or blist -> get_bool_from_directions (blist.[0]) rest
                    | _ -> failwith "Wrong directions!"
    | x :: rest -> match bool with
                           | And blist -> get_bool_from_directions (blist.[x]) rest
                           | Or blist -> get_bool_from_directions (blist.[x]) rest
                           | _ -> failwith "Wrong directions!"
    | [] -> bool

let rec print_boolean bool =
    match bool with
    | Constant s -> if s = true then "True" else "False"
    | Variable c -> c.ToString()
    | And l -> let bracket_control bool = match bool with
                                          | Or _ -> "(" + (print_boolean bool) + ")"
                                          | And _ -> "(" + (print_boolean bool) + ")"
                                          | _ -> print_boolean bool
               let a = List.map bracket_control l
               List.intersperse "." a |> List.fold (+) "" 
    | Or l -> let bracket_control bool = match bool with
                                          | Or _ -> "(" + (print_boolean bool) + ")"
                                          | And _ -> "(" + (print_boolean bool) + ")"
                                          | _ -> print_boolean bool
              let a = List.map bracket_control l
              List.intersperse "+" a |> List.fold (+) "" 
    | Not b -> match b with
               | Variable s -> "!" + (s.ToString())
               | _ -> "!(" + print_boolean b + ")"
    | Highlight b -> "[[" + (print_boolean b) + "]]"

let rec print_boolean_OLD bool =
    match bool with
    | Constant s -> if s = true then "True" else "False"
    | Variable c -> c.ToString()
    | And l -> let or_control bool = match bool with
                                     | Or _ -> "(" + (print_boolean bool) + ")"
                                     | And _ -> "(" + (print_boolean bool) + ")"
                                     | _ -> print_boolean bool
               let a = List.map or_control l
               List.intersperse "." a |> List.fold (+) "" 
    | Or l -> let a = List.map print_boolean l
              List.intersperse "+" a |> List.fold (+) "" 
    | Not b -> "not(" + (print_boolean b) + ")"
    | Highlight b -> "[[" + (print_boolean b) + "]]"

// movement code

let rec followDirections dirs bool =
    match dirs with
    | 0 :: rest -> match bool with
                   | Not(b) -> followDirections rest b
                   | Or(bools)
                   | And(bools) -> followDirections rest (bools.[0])
                   | Constant b -> printf "Followed Wrong Directions!!!!\n" 
                                   bool
                   | Variable(a) -> printf "Followed Wrong Directions!!!!\n" 
                                    bool
    | x :: rest -> match bool with
                   | Not(b) -> printf "Followed Wrong Directions!!!!\n" 
                               bool
                   | Or(bools)
                   | And(bools) -> followDirections rest (bools.[x])
                   | Constant b -> printf "Followed Wrong Directions!!!!\n" 
                                   bool
                   | Variable(a) -> printf "Followed Wrong Directions!!!!\n" 
                                    bool
    | _ -> bool

let rec replaceBoolAtDirections dirs bool newBool =
    match dirs with
    | 0 :: rest -> match bool with
                   | Not(b) -> Not(replaceBoolAtDirections rest b newBool)
                   | Or(bools) -> Or(List.replace (replaceBoolAtDirections rest (bools.[0]) newBool) 0 bools)
                   | And(bools) -> And(List.replace (replaceBoolAtDirections rest (bools.[0]) newBool) 0 bools)
                   | Constant b -> printf "Followed Wrong Directions!!!!\n" 
                                   newBool
                   | Variable(a) -> printf "Followed Wrong Directions!!!!\n" 
                                    newBool
    | x :: rest -> match bool with
                   | Not(b) -> printf "Followed Wrong Directions!!!!\n" 
                               bool
                   | Or(bools) -> Or(List.replace (replaceBoolAtDirections rest (bools.[x]) newBool) x bools)
                   | And(bools) -> And(List.replace (replaceBoolAtDirections rest (bools.[x]) newBool) x bools)
                   | Constant b -> printf "Followed Wrong Directions!!!!\n"
                                   newBool
                   | Variable(a) -> printf "Followed Wrong Directions!!!!\n"
                                    newBool
    | _ -> newBool

let rec highlightDirections dirs bool =
    match dirs with
    | 0 :: rest -> match bool with
                   | Not(b) -> Not(highlightDirections rest b)
                   | Or(bools) -> Or(List.replace (highlightDirections rest (bools.[0])) 0 bools)
                   | And(bools) -> And(List.replace (highlightDirections rest (bools.[0])) 0 bools)
                   | Constant b -> printf "Followed Wrong Directions!!!!\n" 
                                   bool
                   | Variable(a) -> printf "Followed Wrong Directions!!!!\n" 
                                    bool
    | x :: rest -> match bool with
                   | Not(b) -> printf "Followed Wrong Directions!!!!\n" 
                               bool
                   | Or(bools) -> Or(List.replace (highlightDirections rest (bools.[x])) x bools)
                   | And(bools) -> And(List.replace (highlightDirections rest (bools.[x])) x bools)
                   | Constant b -> printf "Followed Wrong Directions!!!!\n"
                                   Highlight bool
                   | Variable(a) -> printf "Followed Wrong Directions!!!!\n"
                                    Highlight bool
    | _ -> Highlight bool

let getPossibleMovements includeOut totalBool dirs =
    // If dirs length is greater than 0,
    // The user is able to come further out
    // the expression.
    let out = match dirs with
              | [] -> []
              | l -> l.[0..(List.length l - 2)]
    let ins = match followDirections dirs totalBool with
              | Not(_) -> [List.append dirs [0]]
              | And(list)
              | Or(list) -> List.map (fun x -> List.append dirs [x]) [0..(List.length list - 1)]
              | _ -> []
    if includeOut && List.length dirs > 0
    then List.append [out] ins
    else ins


// ---------